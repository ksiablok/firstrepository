"""
Функция if_3_do.

Принимает число.
Если оно больше 3, то увеличить число на 10, иначе уменьшить на 10.
Вернуть результат.
"""


def if_3_do(num_basic):
    if num_basic > 3:
        return num_basic + 10
    else:
        return num_basic - 10


if __name__ == '__main__':
    num_new = if_3_do(5)
    print(num_new)
