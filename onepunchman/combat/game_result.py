from enum import Enum, auto


class GameResult(Enum):
    WIN = auto()
    DEFEAT = auto()
    DEADHEAT = auto()

