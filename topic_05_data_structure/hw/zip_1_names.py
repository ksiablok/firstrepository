"""
Функция zip_names.

Принимает 2 аргумента: список с именами и множество с фамилиями.

Возвращает список с парами значений из каждого аргумента.

Если вместо list передано что-то другое, то возвращать строку 'First arg must be list!'.
Если вместо set передано что-то другое, то возвращать строку 'Second arg must be set!'.

Если list пуст, то возвращать строку 'Empty list!'.
Если set пуст, то возвращать строку 'Empty set!'.

Если list и set различного размера, обрезаем до минимального (стандартный zip).
"""


def zip_names(f_names: list, l_names: set):
    if type(f_names) != list:
        return 'First arg must be list!'
    if type(l_names) != set:
        return 'Second arg must be set!'
    if len(f_names) == 0:
        return 'Empty list!'
    if len(l_names) == 0:
        return 'Empty set!'
    return list(zip(f_names, l_names))


if __name__ == '__main__':

    print(zip_names(["John", "Carl", "Bernard"], {"Smith", "Ivanov", "Bing"}))
