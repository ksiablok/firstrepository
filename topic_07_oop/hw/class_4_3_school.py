"""
Класс School.

Поля:
список людей в школе (общий list для Pupil и Worker): people,
номер школы: number.

Методы:
get_avg_mark: вернуть средний балл всех учеников школы
get_avg_salary: вернуть среднюю зп работников школы
get_worker_count: вернуть сколько всего работников в школе
get_pupil_count: вернуть сколько всего учеников в школе
get_pupil_names: вернуть все имена учеников (с повторами, если есть)
get_unique_worker_positions: вернуть список всех должностей работников (без повторов, подсказка: set)
get_max_pupil_age: вернуть возраст самого старшего ученика
get_min_worker_salary: вернуть самую маленькую зп работника
get_min_salary_worker_names: вернуть список имен работников с самой маленькой зп
(список из одного или нескольких элементов)
"""
from topic_07_oop.hw.class_4_1_pupil import Pupil
from topic_07_oop.hw.class_4_2_worker import Worker


class School:
    def __init__(self, people: list, number: int):
        self.people = people
        self.number = number

    def get_avg_mark(self):  # вернуть средний балл всех учеников школы
        all_m = [i.get_avg_mark() for i in self.people if isinstance(i, Pupil)]
        sum_m = sum(all_m)
        return sum_m / len(all_m)

    def get_avg_salary(self):  # вернуть среднюю зп работников школы
        all_s = [s.salary for s in self.people if isinstance(s, Worker)]
        sum_s = sum(all_s)
        return sum_s / len(all_s)

    def get_worker_count(self):  # вернуть сколько всего работников в школе
        return len([w for w in self.people if isinstance(w, Worker)])

    def get_pupil_count(self):  # вернуть сколько всего учеников в школе
        return len([p for p in self.people if isinstance(p, Pupil)])

    def get_pupil_names(self):  # вернуть все имена учеников (с повторами, если есть)
        return [n.name for n in self.people if isinstance(n, Pupil)]

    def get_unique_worker_positions(self):  # вернуть список всех должностей работников (без повторов, подсказка: set)
        return set([p.position for p in self.people if isinstance(p, Worker)])

    def get_max_pupil_age(self):  # вернуть возраст самого старшего ученика
        return max([a.age for a in self.people if isinstance(a, Pupil)])

    def get_min_worker_salary(self):  # вернуть самую маленькую зп работника
        return min([s.salary for s in self.people if isinstance(s, Worker)])

    def get_min_salary_worker_names(self):  # вернуть список имен работников с самой маленькой зп
        return [n.name for n in self.people if isinstance(n, Worker) if n.salary == self.get_min_worker_salary()]


if __name__ == '__main__':
    pupil1 = Pupil("Кейт", 8, {'math': [3, 5], 'english': [5, 5, 4]})
    pupil2 = Pupil("Петр", 9, {'german': [4, 3], 'english': [3, 2, 4]})
    worker1 = Worker("Джон", 12000, "teacher")
    worker2 = Worker("Ирина Витальевна", 15000, "teacher")
    school1 = School([pupil1, pupil2, worker1, worker2], 123)

    print(School.get_avg_mark(school1))
    print(School.get_avg_salary(school1))
    print(School.get_worker_count(school1))
    print(School.get_pupil_count(school1))
    print(School.get_pupil_names(school1))
    print(School.get_unique_worker_positions(school1))
    print(School.get_max_pupil_age(school1))
    print(School.get_min_worker_salary(school1))
    print(School.get_min_salary_worker_names(school1))
