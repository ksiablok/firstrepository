"""
Функция list_to_str.

Принимает 2 аргумента: список и разделитель (строка).

Возвращает (строку полученную разделением элементов списка разделителем,
количество разделителей в получившейся строке в квадрате).

Если вместо списка передано что-то другое, то возвращать строку 'First arg must be list!'.

Если разделитель не является строкой, то возвращать строку 'Second arg must be str!'.

Если список пуст, то возвращать пустой tuple().

ВНИМАНИЕ: в списке могут быть элементы любого типа (нужно конвертировать в строку).
"""


def list_to_str(list1: list, separator: str):
    if type(list1) != list:
        return 'First arg must be list!'
    if type(separator) != str:
        return 'Second arg must be str!'
    if len(list1) == 0:
        return tuple()
    new_str = separator.join(str(i) for i in list1)
    # new_str = separator.join(map(str, list1))
    return new_str, new_str.count(separator) ** 2


if __name__ == '__main__':
    # print(list_to_str([1, 2, 5, "Cat", "Fox"], "-"))
    print(list_to_str([1, '2', 'awe', [1, 2, 3]], "-"))