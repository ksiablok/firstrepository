"""
Функция dict_to_list.

Принимает 1 аргумент: словарь.

Возвращает список: [
список ключей,
список значений,
количество уникальных элементов в списке ключей,
количество уникальных элементов в списке значений
].

Если вместо словаря передано что-то другое, то возвращать строку 'Must be dict!'.

Если dict пуст, то возвращать ([], [], 0, 0).
"""


def dict_to_list(my_dict: dict):
    if type(my_dict) != dict:
        return 'Must be dict!'
    if not my_dict:
        return [], [], 0, 0

    key_list = list(my_dict.keys())
    value_list = list(my_dict.values())
    c_unique_keys = len(set(key_list))
    c_unique_values = len(set(value_list))

    return key_list, value_list, c_unique_keys, c_unique_values


if __name__ == '__main__':
    print(dict_to_list({1: 3, 2: 3, 3: 1, 4: 7}))
