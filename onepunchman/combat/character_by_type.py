from onepunchman.combat.character_type import CharacterType

character_by_type = {
    CharacterType.CLASS_S: {"Генос": "Genos.png",
                            "Гомо Гомо Зек": "Homo.png",
                            "Тацумаки": "Tatsumaki.png"},
    CharacterType.CLASS_A: {"Милая маска": "Masuku.jpg",
                            "Пулемет смерти": "Pulemet.png",
                            "Тяжелый танк Фундоши": "Tank.png"},
    CharacterType.CLASS_B: {"Реактивный добряк": "Dobriak.png",
                            "Гриб": "Mushroom.png",
                            "Ананас": "Ananas.jpg"},
    CharacterType.CLASS_C: {"Безлицензионный Ездок": "Ezdok.png",
                            "Тигр в майке": "Tigr.png",
                            "Геймпад": "Geimpad.png"},
    CharacterType.SAITAMA: {"Сайтама": "Saitama.jpg"},
    CharacterType.TIGER: {"Земляной Дракон": "drakon.png",
                          "Маршалл Горилла": "marshal.jpg",
                          "Крабинатор": "crab.jpg"},
    CharacterType.DEMON: {"Царь Зверей": "Beast_King.png",
                          "Бронированная Горилла": "Gorilla.png",
                          "Комариха": "Mosquito-girl.png"},
    CharacterType.DRAGON: {"Борос": "boros.png",
                           "Мельзальгальд": "melzargagd.jpg",
                           "Гокецу": "gouketsu.png"},
    CharacterType.INDEPENDENT: {"Гароу": "Garo.png",
                                "Сверхзвуковой Соник": "Sonic.png"}

}
