from onepunchman.combat.strike_target import StrikeTarget
from onepunchman.combat.character_state import CharacterState
from onepunchman.combat.character_type import CharacterType
from onepunchman.combat.character_types_weakspots import character_defence_weakspots_by_type as weakspots_by_type


class Character:
    def __init__(self,
                 name: str,
                 character_type: CharacterType):
        self.name = name
        self.character_type = character_type
        self.weakspots = weakspots_by_type.get(character_type, tuple())
        self.lp = 100
        self.attack_target = None
        self.defence_target = None
        self.strike_power = 20
        self.state = CharacterState.READY

    def __str__(self):
        return f"Name: {self.name} | Type: {self.character_type.name}\nLife Points: {self.lp}"

    def next_step_points(self,
                         next_attack: StrikeTarget,
                         next_defence: StrikeTarget):
        self.attack_target = next_attack
        self.defence_target = next_defence

    def get_hit(self,
                enemy_attack_target: StrikeTarget,
                enemy_strike_power: int,
                enemy_type: CharacterType):
        if enemy_attack_target == StrikeTarget.NIX:
            return "И это все,что ты можешь? :)"
        elif self.defence_target == enemy_attack_target:
            return "Ты не пройдешь!"
        else:
            self.lp -= enemy_strike_power * (3 if enemy_type in self.weakspots else 1)

            if self.lp <= 0:
                self.state = CharacterState.DEFEATED
                return "Ушел с позором из этого мира"
            else:
                return "Я тебе еще покажу!"


if __name__ == '__main__':
    сhr1 = Character('Dragon', CharacterType.DRAGON)
    print(сhr1)
