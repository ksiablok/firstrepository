from enum import Enum, auto


class StrikeTarget(Enum):
    NIX = auto()
    NODDLE = auto()
    BELLY = auto()
    LIMBS = auto()

    @classmethod
    def min_value(cls):
        return cls.NIX.value

    @classmethod
    def max_value(cls):
        return cls.LIMBS.value

    @classmethod
    def has_item(cls, name: str):
        return name in cls._member_names_
