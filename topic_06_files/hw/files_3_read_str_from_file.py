"""
Функция read_str_from_file.

Принимает 1 аргумент: строка (название файла или полный путь к файлу).

Выводит содержимое файла (в консоль). (* Файл должен быть предварительно создан и наполнен каким-то текстом.)
"""


def read_str_from_file(path):
    with open(path, "r") as f:
        for line in f:
            print(line)


if __name__ == '__main__':
    my_path = "my_dict.txt"
    read_str_from_file(my_path)
