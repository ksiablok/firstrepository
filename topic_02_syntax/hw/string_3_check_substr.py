"""
Функция check_substr.

Принимает две строки.
Если меньшая по длине строка содержится в большей, то возвращает True,
иначе False.
Если строки равны, то False.
Если одна из строк пустая, то True.
"""


def check_substr(str_1, str_2):
    if str_1 == str_2:
        return False
    elif str_1 == "" or str_2 == "":
        return True
    elif (len(str_1) < len(str_2) and str_2.count(str_1) > 0) or (len(str_2) < len(str_1) and str_1.count(str_2) > 0):
        return True
    else:
        return False


if __name__ == '__main__':
    print(check_substr('qwe', 'qe'))
