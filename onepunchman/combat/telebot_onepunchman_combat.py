import json

import telebot
from telebot import types
from telebot.types import Message

from onepunchman.combat.character_npc import CharacterNPC
from onepunchman.combat.strike_target import StrikeTarget
from onepunchman.combat.character import Character
from onepunchman.combat.character_by_type import character_by_type
from onepunchman.combat.character_state import CharacterState
from onepunchman.combat.character_type import CharacterType
from onepunchman.combat.game_result import GameResult

with open("./bot_token.txt", 'r') as token_file:
    token = token_file.read().strip()

bot = telebot.TeleBot(token)

state = {}

statistics = {}

stat_file = "game_stat.json"

strike_target_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True,
                                                   one_time_keyboard=True,
                                                   row_width=len(StrikeTarget))

strike_target_keyboard.row(*[types.KeyboardButton(strike_target.name) for strike_target in StrikeTarget])


def update_save_stat(chat_id, result: GameResult):
    print("Обновляем статистику", end="...")
    global statistics

    chat_id = str(chat_id)

    if statistics.get(chat_id, None) is None:
        statistics[chat_id] = {}

    if result == GameResult.WIN:
        statistics[chat_id]['WIN'] = statistics[chat_id].get('WIN', 0) + 1
    elif result == GameResult.DEFEAT:
        statistics[chat_id]['DEFEAT'] = statistics[chat_id].get('DEFEAT', 0) + 1
    elif result == GameResult.DEADHEAT:
        statistics[chat_id]['DEADHEAT'] = statistics[chat_id].get('DEADHEAT', 0) + 1
    else:
        print(f"Нет такого результата {result}")

    with open(stat_file, 'w') as file:
        json.dump(statistics, file)

    print("Финиш!")


def load_stat():
    print('Загружаем статистику...', end='')
    global statistics

    try:
        with open(stat_file, 'r') as file:
            statistics = json.load(file)
        print('Загрузка завершена успешно!')
    except FileNotFoundError:
        statistics = {}
        print('Указанный файл не найден!')


@bot.message_handler(commands=["help", "info"])
def help_command(message):
    bot.send_message(message.chat.id,
                     "Привет!\n/start для начала игры\n/stat для отображения статистики")


@bot.message_handler(commands=["stat"])
def stat(message):
    global statistics
    if statistics.get(str(message.chat.id), None) is None:
        user_stat = "Мы еще не играли"
    else:
        user_stat = "Твои успехи:"
        for res, num in statistics[str(message.chat.id)].items():
            user_stat += f"\n{res}: {num}"

    bot.send_message(message.chat.id,
                     text=user_stat)


@bot.message_handler(commands=["start"])
def start(message):
    yes_no_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True,
                                                one_time_keyboard=True,
                                                row_width=2)
    yes_no_keyboard.row("Да", "Нет")

    bot.send_message(message.from_user.id,
                     text="Готов начать битву монстров и героев?",
                     reply_markup=yes_no_keyboard)

    bot.register_next_step_handler(message, start_question_handler)


def start_question_handler(message):
    if message.text.lower() == 'да':
        bot.send_message(message.from_user.id,
                         'И...Поехали!!')

        create_npc(message)

        ask_user_about_character_type(message)

    elif message.text.lower() == 'нет':
        bot.send_message(message.from_user.id,
                         'Твоя воля, дай знать, как захочешь.')
    else:
        bot.send_message(message.from_user.id,
                         'Что то ты попутал!')


def create_npc(message):
    print(f"Начинаем создавать объект NPC для chat id = {message.chat.id}")
    global state
    character_npc = CharacterNPC()
    if state.get(message.chat.id, None) is None:
        state[message.chat.id] = {}
    state[message.chat.id]['npc_character'] = character_npc

    npc_image_filename = character_by_type[character_npc.character_type][character_npc.name]
    bot.send_message(message.chat.id, 'Твой враг:')
    with open(f"../images/{npc_image_filename}", 'rb') as file:
        bot.send_photo(message.chat.id, file, character_npc)
    print(f"Завершаем создание объекта NPC для chat id = {message.chat.id}")


def ask_user_about_character_type(message):
    markup = types.InlineKeyboardMarkup()

    for character_type in CharacterType:
        markup.add(types.InlineKeyboardButton(text=character_type.name,
                                              callback_data=f"character_type_{character_type.value}"))

    bot.send_message(message.chat.id, "Выбери своего персонажа:", reply_markup=markup)


@bot.callback_query_handler(func=lambda call: "character_type_" in call.data)
def character_type_handler(call):
    call_data_split = call.data.split("_")
    if len(call_data_split) != 3 or not call_data_split[2].isdigit():
        bot.send_message(call.message.chat.id, "Требуется перезапуск сессии!")
    else:
        character_type_id = int(call_data_split[2])

        bot.send_message(call.message.chat.id, "Выбери своего персонажа:")

        ask_user_about_character_by_type(character_type_id, call.message)


def ask_user_about_character_by_type(character_type_id, message):
    character_type = CharacterType(character_type_id)
    character_dict_by_type = character_by_type.get(character_type, {})
    for character_name, character_img in character_dict_by_type.items():
        markup = types.InlineKeyboardMarkup()
        markup.add(types.InlineKeyboardButton(text=character_name,
                                              callback_data=f"character_name_{character_type_id}_{character_name}"))
        with open(f"../images/{character_img}", 'rb') as file:
            bot.send_photo(message.chat.id, file, reply_markup=markup)


@bot.callback_query_handler(func=lambda call: "character_name_" in call.data)
def character_name_handler(call):
    call_data_split = call.data.split("_")
    if len(call_data_split) != 4 or not call_data_split[2].isdigit():
        bot.send_message(call.message.chat.id, "Требуется перезапуск сессии!")
    else:
        character_type_id, character_name = int(call_data_split[2]), call_data_split[3]

        create_user_character(call.message, character_type_id, character_name)

        bot.send_message(call.message.chat.id, "Начинаем битву героев и монстров!")

        game_next_step(call.message)


def create_user_character(message, character_type_id, character_name):
    print(f"Начинаем создание объекта Сharacter для chat id = {message.chat.id}")
    global state
    user_character = Character(name=character_name,
                               character_type=CharacterType(character_type_id))

    if state.get(message.chat.id, None) is None:
        state[message.chat.id] = {}
    state[message.chat.id]['user_character'] = user_character

    image_filename = character_by_type[user_character.character_type][user_character.name]
    bot.send_message(message.chat.id, 'Твой выбор:')
    with open(f"../images/{image_filename}", 'rb') as file:
        bot.send_photo(message.chat.id, file, user_character)

    print(f"Завершаем создание объекта Сharacter для chat id = {message.chat.id}")


def game_next_step(message: Message):
    bot.send_message(message.chat.id,
                     "Выбери точку защиты:",
                     reply_markup=strike_target_keyboard)

    bot.register_next_step_handler(message, reply_defend)


def reply_defend(message: Message):
    if not StrikeTarget.has_item(message.text):
        bot.send_message(message.chat.id, "Используй клавиатуру!")
        game_next_step(message)
    else:
        bot.send_message(message.chat.id,
                         "Выбери точку для удара:",
                         reply_markup=strike_target_keyboard)

        bot.register_next_step_handler(message, reply_attack, defend_strike_target=message.text)


def reply_attack(message: Message, defend_strike_target: str):
    if not StrikeTarget.has_item(message.text):
        bot.send_message(message.chat.id, "Используй клавиатуру!")
        game_next_step(message)
    else:
        attack_strike_target = message.text

        global state

        user_character = state[message.chat.id]['user_character']

        character_npc = state[message.chat.id]['npc_character']

        user_character.next_step_points(next_attack=StrikeTarget[attack_strike_target],
                                        next_defence=StrikeTarget[defend_strike_target])

        character_npc.next_step_points()

        game_step(message, user_character, character_npc)


def game_step(message: Message, user_character: Character, character_npc: Character):
    comment_npc = character_npc.get_hit(enemy_attack_target=user_character.attack_target,
                                        enemy_strike_power=user_character.strike_power,
                                        enemy_type=user_character.character_type)
    bot.send_message(message.chat.id, f"Enemy: {comment_npc}\nLife Points: {character_npc.lp}")

    comment_user = user_character.get_hit(enemy_attack_target=character_npc.attack_target,
                                          enemy_strike_power=character_npc.strike_power,
                                          enemy_type=character_npc.character_type)
    bot.send_message(message.chat.id, f"Your character: {comment_user}\nLife Points: {user_character.lp}")

    if character_npc.state == CharacterState.READY and user_character.state == CharacterState.READY:
        bot.send_message(message.chat.id, "Продолжаем игру!")
        game_next_step(message)
    elif character_npc.state == CharacterState.DEFEATED and user_character.state == CharacterState.DEFEATED:
        bot.send_message(message.chat.id, "Вот это да! у вас ничья! Помни: Зло из нашего мира не исчезнет. "
                                          "Оно всегда было, есть и будет. "
                                          "Другими словами, твои действия никак не влияют на мир.")
        update_save_stat(message.chat.id, GameResult.DEADHEAT)
    elif character_npc.state == CharacterState.DEFEATED:
        bot.send_message(message.chat.id, "Ты победил! Дьявольски скучно обладать могучей силой.")
        update_save_stat(message.chat.id, GameResult.WIN)
    elif user_character.state == CharacterState.DEFEATED:
        bot.send_message(message.chat.id, "Ты проиграл! Кто мало тренируется — тот мало живет. Попробуй еще раз")
        update_save_stat(message.chat.id, GameResult.DEFEAT)


if __name__ == '__main__':
    load_stat()

    print('Starting the bot...')
    bot.polling(none_stop=True, interval=0)
    print('The bot has stopped!')
