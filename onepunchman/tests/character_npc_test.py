import random

from onepunchman.combat.character_npc import CharacterNPC
from onepunchman.combat.character_state import CharacterState
from onepunchman.combat.character_type import CharacterType
from onepunchman.combat.character_types_weakspots import character_defence_weakspots_by_type as weakspots_by_type


class TestCharacterNPCClass:
    character_name = 'Тигр в майке'
    character_type = CharacterType.CLASS_C
    max_lp = 100

    def setup_method(self, method):
        random.seed(123)

    def test_init(self):
        character_test = CharacterNPC()

        assert character_test.name == self.__class__.character_name
        assert character_test.character_type == self.__class__.character_type
        assert character_test.weakspots == weakspots_by_type[self.__class__.character_type]
        assert character_test.lp == self.__class__.max_lp
        assert character_test.attack_target is None
        assert character_test.defence_target is None
        assert character_test.strike_power == 20
        assert character_test.state == CharacterState.READY

    def test_str(self):
        character_test = CharacterNPC()
        assert str(character_test) == f"Name: {self.__class__.character_name} | " \
                                      f"Type: {self.__class__.character_type.name}\n" \
                                      f"Life Points: {self.__class__.max_lp}"
