"""
Функция save_dict_to_file_pickle.

Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).

Сохраняет список в файл. Загрузить словарь и проверить его на корректность.
"""
import pickle


def save_dict_to_file_pickle(str_f, dict_f):
    with open(str_f, 'wb') as file_f:
        pickle.dump(dict_f, file_f)


if __name__ == '__main__':
    my_dict = {1: 'cat', 2: 'dog', 3: 'pig'}
    path = 'dict.pkl'

    save_dict_to_file_pickle(path, my_dict)

    with open(path, 'rb') as f:
        dict_loaded = pickle.load(f)

    # проверка
    print(f'type: {type(dict_loaded)}')
    print(f'equal: {my_dict == dict_loaded}')
