"""
Функция magic_reversal.

Принимает 1 аргумент: список my_list.

Создает новый список new_list (копия my_list), порядок которого обратный my_list.

Возвращает список, который состоит из
[второго элемента (индекс=1) new_list]
+ [предпоследнего элемента new_list]
+ [весь new_list].

Пример:
входной список [1,  'aa', 99]
new_list [99, 'aa', 1]
результат ['aa', 'aa', 99, 'aa', 1].

Если список состоит из одного элемента, то "предпоследний" = единственный и "второй элемент" = единственный.

Если вместо списка передано что-то другое, то возвращать строку 'Must be list!'.
Если список пуст, то возвращать строку 'Empty list!'.

ВНИМАНИЕ: Изначальный список не должен быть изменен!
"""


def magic_reversal(my_list: list):
    if type(my_list) != list:
        return 'Must be list!'
    if len(my_list) == 0:
        return 'Empty list!'
    new_list = []
    new_list.extend(my_list)
    new_list.reverse()
    res_l = []
    if len(new_list) != 1:
        sec_el_l = new_list[1]
        pen_el_l = new_list[-2]
        whole_l = []
        whole_l.extend(new_list)
    else:
        sec_el_l = new_list[0]
        pen_el_l = new_list[0]
        whole_l = []
        whole_l.extend(new_list)
    res_l.append(sec_el_l)
    res_l.append(pen_el_l)
    res_l.extend(whole_l)
    return res_l


if __name__ == '__main__':
    print(magic_reversal([1,  'aa', 99]))
