"""
Класс Pupil.

Поля:
имя: name,
возраст: age,
dict с оценками: marks (пример: {'math': [3, 5], 'english': [5, 5, 4] ...].

Методы:
get_all_marks: получить список всех оценок,
get_avg_mark_by_subject: получить средний балл по предмету (если предмета не существует, то вернуть 0),
get_avg_mark: получить средний балл (все предметы),
__le__: вернуть результат сравнения (<=) средних баллов (все предметы) двух учеников.
"""


class Pupil:
    def __init__(self, n, a, marks):
        self.name = n
        self.age = a
        self.marks = marks

    def get_all_marks(self):
        marks_list = []
        for m in list(self.marks.values()):
            marks_list.extend(m)
        return marks_list

    def get_avg_mark_by_subject(self, i):
        if i in self.marks.keys():
            i_list = self.marks.get(i, ())
            return sum(i_list)/len(i_list)
        else:
            return 0

    def get_avg_mark(self):
        marks_list = self.get_all_marks()
        return sum(marks_list)/len(marks_list) if marks_list else 0

    def __le__(self, other):
        return self.get_avg_mark() <= other.get_avg_mark()


if __name__ == '__main__':
    pupil1 = Pupil("Кейт", 8, {'math': [3, 5], 'english': [5, 5, 4]})
    pupil2 = Pupil("Петр", 9, {'german': [4, 3], 'english': [3, 2, 4]})
    print(pupil1.get_all_marks())
    print(pupil1.get_avg_mark_by_subject("math"))
    print(pupil1.get_avg_mark())
