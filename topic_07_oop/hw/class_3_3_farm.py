"""
Класс Farm.

Поля:
животные (list из произвольного количества Goat и Chicken): zoo_animals
(вначале список пуст, потом добавялем животных методами append и extend самостоятельно),
наименование фермы: name,
имя владельца фермы: owner.

Методы:
get_goat_count: вернуть количество коз на ферме (*Подсказка isinstance или type == Goat),
get_chicken_count: вернуть количество куриц на ферме,
get_animals_count: вернуть количество животных на ферме,
get_milk_count: вернуть сколько молока можно получить в день,
get_eggs_count: вернуть сколько яиц можно получить в день.
"""
from topic_07_oop.hw.class_3_1_chicken import Chicken
from topic_07_oop.hw.class_3_2_goat import Goat


class Farm:
    def __init__(self, n: str, o: str):
        self.zoo_animals = []
        self.name = n
        self.owner = o

    def get_goat_count(self):
        return len([g for g in self.zoo_animals if isinstance(g, Goat)])

    def get_chicken_count(self):
        return len([c for c in self.zoo_animals if isinstance(c, Chicken)])

    def get_animals_count(self):
        return len(self.zoo_animals)

    def get_milk_count(self):
        return sum([m.milk_per_day for m in self.zoo_animals if isinstance(m, Goat)])

    def get_eggs_count(self):
        return sum([e.eggs_per_day for e in self.zoo_animals if isinstance(e, Chicken)])


if __name__ == '__main__':
    farm1 = Farm("Farmeevo", "Grigory")
    farm1.zoo_animals.extend([Chicken("Катя", 3, 5),
                              Chicken("Валя", 5, 2),
                              Chicken("Нелли", 4, 4),
                              Goat("Инесс", 8, 9),
                              Goat("Прешс", 6, 7),
                              Goat("Даймонд", 3, 5)])
    print(farm1.get_goat_count())
    print(farm1.get_chicken_count())
    print(farm1.get_animals_count())
    print(farm1.get_milk_count())
    print(farm1.get_eggs_count())
