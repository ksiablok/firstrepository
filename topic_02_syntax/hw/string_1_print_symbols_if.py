"""
Функция print_symbols_if.

Принимает строку.

Если строка нулевой длины, то вывести строку "Empty string!".

Если длина строки больше 5, то вывести первые три символа и последние три символа.
Пример: string='123456789' => result='123789'

Иначе вывести первый символ столько раз, какова длина строки.
Пример: string='345' => result='333'
"""


def print_symbols_if(string1):
    string1_length = len(string1)
    if string1_length == 0:
        print("Empty string!")
    elif string1_length > 5:
        print(string1[0:3] + string1[-3:])
    else:
        print(string1[0] * string1_length)


if __name__ == '__main__':
    print_symbols_if('345')
