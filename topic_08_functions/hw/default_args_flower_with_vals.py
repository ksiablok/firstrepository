"""
Функция flower_with_default_vals.

Принимает 3 аргумента:
цветок (по умолчанию "ромашка"),
цвет (по умолчанию "белый"),
цена (по умолчанию 10.25).

Функция flower_with_default_vals выводит строку в формате
"Цветок: <цветок> | Цвет: <цвет> | Цена: <цена>".

При этом в функции flower_with_default_vals есть проверка на то, что цветок и цвет - это строки

(* Подсказка: type(x) == str или isinstance(s, str)), а также цена - это число больше 0, но меньше 10000.
В функции main вызвать функцию flower_with_default_vals различными способами
(перебрать варианты: цветок, цвет, цена, цветок цвет, цветок цена, цвет цена, цветок цвет цена).

(* Использовать именованные аргументы).
"""


def flower_with_default_vals(flower="Ромашка", color="белый", price=10.25):
    if type(flower) != str or type(color) != str:
        print("Must be str!")
    elif price < 0 or price > 10000:
        print("Must be >0 or <10000")
    else:
        print(f'Цветок: {flower}| Цвет: {color}| Цена: {price}')


if __name__ == '__main__':
    flower_with_default_vals(flower="Роза")
    flower_with_default_vals(color="Розовый")
    flower_with_default_vals(price=32)
    flower_with_default_vals(flower="Нарцисс", color="Желтый")
    flower_with_default_vals(flower="Гвоздика", price=15)
    flower_with_default_vals(color="Синий", price=100)
    flower_with_default_vals(flower="Лизиантус", color="Фиолетовый", price=150)
