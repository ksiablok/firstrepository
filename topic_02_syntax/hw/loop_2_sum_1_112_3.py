"""
Функция sum_1_112_3.

Вернуть сумму 1+4+7+10+...109+112.
"""


def sum_1_112_3():
    i = 1
    num_sum = 0
    while i < 113:
        num_sum = num_sum + i
        i += 3
    return num_sum


if __name__ == '__main__':
    print(sum_1_112_3())
