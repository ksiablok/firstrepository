import random

from onepunchman.combat.strike_target import StrikeTarget
from onepunchman.combat.character import Character
from onepunchman.combat.character_by_type import character_by_type
from onepunchman.combat.character_type import CharacterType


class CharacterNPC(Character):
    def __init__(self):
        rand_type_value = random.randint(CharacterType.min_value(), CharacterType.max_value())

        rand_character_type = CharacterType(rand_type_value)
        rand_character_name = random.choice(list(character_by_type.get(rand_character_type, {}).keys()))

        super().__init__(rand_character_name, rand_character_type)

    def next_step_points(self, **kwargs):
        attack_target = StrikeTarget(random.randint(StrikeTarget.min_value(), StrikeTarget.max_value()))
        defence_target = StrikeTarget(random.randint(StrikeTarget.min_value(), StrikeTarget.max_value()))
        super().next_step_points(next_attack=attack_target,
                                 next_defence=defence_target)


if __name__ == '__main__':
    character_npc = CharacterNPC()
    character_npc.next_step_points()
    print(character_npc)
