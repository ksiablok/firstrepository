"""
Функция magic_mul.

Принимает 1 аргумент: список my_list.

Возвращает список, который состоит из
[первого элемента my_list]
+ [три раза повторенных списков my_list]
+ [последнего элемента my_list].

Пример:
входной список [1,  'aa', 99]
результат [1, 1,  'aa', 99, 1,  'aa', 99, 1,  'aa', 99, 99].

Если вместо списка передано что-то другое, то возвращать строку 'Must be list!'.
Если список пуст, то возвращать строку 'Empty list!'.
"""


def magic_mul(my_list: list):
    if type(my_list) != list:
        return 'Must be list!'
    if len(my_list) == 0:
        return 'Empty list!'
    result_l = []
    first_l = my_list[:1]
    repeat_l = my_list*3
    last_l = my_list[-1]
    result_l.extend(first_l)
    result_l.extend(repeat_l)
    result_l.append(last_l)
    return result_l


if __name__ == '__main__':
    print(magic_mul([1,  'aa', 99]))
