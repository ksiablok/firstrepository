"""
Функция get_words_by_translation.

Принимает 2 аргумента:
ru-eng словарь содержащий {ru_word: [eng_1, eng_2 ...], ...}
слово для поиска в словаре (eng).

Возвращает список всех вариантов переводов (ru), если такое слово есть в словаре (eng),
если нет, то "Can't find English word: {word}".

Если вместо словаря передано что-то другое, то возвращать строку "Dictionary must be dict!".
Если вместо строки для поиска передано что-то другое, то возвращать строку "Word must be str!".

Если словарь пустой, то возвращать строку "Dictionary is empty!".
Если строка для поиска пустая, то возвращать строку "Word is empty!".
"""


def get_words_by_translation(my_dict: dict, en_word: str):
    if type(my_dict) != dict:
        return "Dictionary must be dict!"
    if type(en_word) != str:
        return "Word must be str!"
    if len(my_dict) == 0:
        return "Dictionary is empty!"
    if len(en_word) == 0:
        return "Word is empty!"
    ru_words = []
    for ru_w, en_ws in my_dict.items():
        if en_word in en_ws:
            ru_words.append(ru_w)
    return ru_words if len(ru_words) > 0 else f"Can't find English word: {en_word}"


if __name__ == '__main__':
    some_dict = {"проблема": ["problem", "issue"]}
    print(get_words_by_translation(some_dict, "issue"))
