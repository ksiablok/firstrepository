from onepunchman.combat.character import Character
from onepunchman.combat.character_state import CharacterState
from onepunchman.combat.character_type import CharacterType
from onepunchman.combat.character_types_weakspots import character_defence_weakspots_by_type as weakspots_by_type


class TestCharacterClass:
    character_name = 'Генос'
    character_type = CharacterType.CLASS_S
    max_lp = 100

    def test_init(self):
        character_test = Character(name=self.__class__.character_name,
                                   character_type=self.__class__.character_type)

        assert character_test.name == self.__class__.character_name
        assert character_test.character_type == self.__class__.character_type
        assert character_test.weakspots == weakspots_by_type[self.__class__.character_type]
        assert character_test.lp == self.__class__.max_lp
        assert character_test.attack_target is None
        assert character_test.defence_target is None
        assert character_test.strike_power == 20
        assert character_test.state == CharacterState.READY

    def test_str(self):
        character_test = Character(name=self.__class__.character_name,
                                   character_type=self.__class__.character_type)
        assert str(character_test) == f"Name: {self.__class__.character_name} | " \
                                      f"Type: {self.__class__.character_type.name}\n" \
                                      f"Life Points: {self.__class__.max_lp}"
