from enum import Enum, auto


class CharacterType(Enum):
    CLASS_S = auto()
    CLASS_A = auto()
    CLASS_B = auto()
    CLASS_C = auto()
    TIGER = auto()
    DEMON = auto()
    DRAGON = auto()
    INDEPENDENT = auto()
    SAITAMA = auto()

    @classmethod
    def min_value(cls):
        return cls.CLASS_C.value

    @classmethod
    def max_value(cls):
        return cls.SAITAMA.value
