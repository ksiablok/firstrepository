"""
Функция check_sum.

Принимает 3 числа.
Вернуть True, если можно взять какие-то два из них и в сумме получить третье, иначе False
"""


def check_sum(n1, n2, n3):
    if n1 + n2 == n3:
        return True
    elif n2 + n3 == n1:
        return True
    elif n1 + n3 == n2:
        return True
    else:
        return False


if __name__ == '__main__':
    res = check_sum(9, 100, 205)
    print(res)
